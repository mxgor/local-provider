package types

import (
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"strings"

	"github.com/lestrrat-go/jwx/v2/jwk"
)

func (key CryptoKey) GetPem() (p *pem.Block, err error) {
	if !strings.Contains(string(key.KeyType), "aes") {

		block, _ := pem.Decode(key.Key)
		if block == nil {
			return nil, fmt.Errorf("failed to parse PEM block")
		}

		return block, nil

	}
	return nil, errors.New("Unsupported")
}

func (key CryptoKey) GetJwk() (jwk.Key, error) {

	block, _ := pem.Decode(key.Key)
	if block == nil {
		return nil, fmt.Errorf("failed to parse PEM block")
	}

	pubKey, err := x509.ParsePKIXPublicKey(block.Bytes)

	if err != nil {
		return nil, err
	}

	jwk, err := jwk.FromRaw(pubKey)

	if err != nil {
		return nil, err
	}

	err = jwk.Set("kid", key.Identifier.KeyId)

	return jwk, err
}
